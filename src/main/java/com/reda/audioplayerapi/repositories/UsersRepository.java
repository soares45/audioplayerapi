package com.reda.audioplayerapi.repositories;

import org.springframework.data.repository.CrudRepository;

import com.reda.audioplayerapi.models.Users;

public interface UsersRepository extends CrudRepository<Users, Integer> {
	
	Users findByUsername(String username);

}
