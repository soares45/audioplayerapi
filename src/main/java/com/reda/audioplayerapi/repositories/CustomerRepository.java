package com.reda.audioplayerapi.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.reda.audioplayerapi.models.Customer;

public interface CustomerRepository extends JpaRepository<Customer, Integer> {

}
