package com.reda.test.audioplayerapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.reda")
public class AudioPlayerApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(AudioPlayerApiApplication.class, args);
	}
}
